using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-1)]
public class GameState : MonoBehaviour
{
    private static InputManager _input;

    public delegate void Blank();
    public static event Blank OnLose;
    public static event Blank OnPlay;

    private void Awake()
    {
        _input = InputManager.Instance;
    }

    private static void Pause()
    {
        ChangeTimeScale(0f);
        _input.Controls.Disable();
    }

    private static void Unpause()
    {
        ChangeTimeScale(1f);
        _input.Controls.Enable();
    }

    public static void Play()
    {
        OnPlay?.Invoke();
    }

    public static void Lose()
    {
        OnLose?.Invoke();
    }

    public static void ChangeTimeScale(float value)
    {
        Time.timeScale = value;
    }

    public static void ChangeTimeScale(bool value)
    {
        if (!value)
        {
            Unpause();
        }
        else
        {
            Pause();
        }
    }
}
