using UnityEngine;
using UnityEngine.AI;

[DefaultExecutionOrder(-2)]
public class GroundFinder : MonoBehaviour 
{
    [SerializeField]
    private Transform _bottomLeftObject, _topRightObject;

    public static GroundFinder Instance;

    private Transform _transform;
    private Vector3 _bottomLeftPoint, _topRightPoint;

    private void Awake()
    {
        Setup();
    }

    private void Setup()
    {
        _transform = transform;
        Instance = this;

        _bottomLeftPoint = _bottomLeftObject.position;
        _topRightPoint = _topRightObject.position;
    }

    public Vector3 GetNavMeshRandomPoint()
    {
        while (true)
        {
            _transform.position = GetNewRandomPosition();
            if (Physics.Raycast(_transform.position, Vector3.down, out RaycastHit hit, 10f))
            {
                if (PointOnNavMesh(hit.point))
                {
                    return hit.point;
                }
            }
        }
    }

    private Vector3 GetNewRandomPosition()
    {
        Vector3 randomPosition;
        randomPosition.x = Random.Range(_bottomLeftPoint.x, _topRightPoint.x);
        randomPosition.y = 7f;
        randomPosition.z = Random.Range(_bottomLeftPoint.z, _topRightPoint.z);
        return randomPosition;
    }

    private bool PointOnNavMesh(Vector3 point)
    {
        if (NavMesh.SamplePosition(point, out _, 0.1f, NavMesh.AllAreas))
        {
            return true;
        }
        return false;
    }
}
