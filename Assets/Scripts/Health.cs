using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField]
    private int _maxHealth;
    [SerializeField]
    private int _currentHealth;

    #region Events
    public delegate void Blank();
    public event Blank OnHit;
    public event Blank OnDeath;
    #endregion

    private void Start()
    {
        OnDeath += Deactivate;
    }

    public void SetMaxHealth(int maxHealth)
    {
        _maxHealth = maxHealth;
        _currentHealth = _maxHealth;
    }

    private void OnEnable()
    {
        _currentHealth = _maxHealth;
    }

    public void ChangeHealth(int value)
    {
        _currentHealth += value;
        OnHit?.Invoke();
        CheckHealth();
    }

    private void CheckHealth()
    {
        if (_currentHealth < 1)
        {
            OnDeath.Invoke();
        }
    }

    private void Deactivate()
    {
        gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        OnDeath -= Deactivate;
    }
}
