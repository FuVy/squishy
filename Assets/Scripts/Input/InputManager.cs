using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[DefaultExecutionOrder(-1)]
public class InputManager : MonoBehaviour
{
    private PlayerControls _playerControls;

    public PlayerControls Controls => _playerControls;

    #region Events
    public delegate void Tap(Vector2 position);
    public event Tap OnTap;
    #endregion

    public static InputManager Instance;
    private void Awake()
    {
        _playerControls = new PlayerControls();
        Instance = this;
    }

    private void Start()
    {
        Setup();
    }

    private void Setup()
    {
        _playerControls.Touch.PointerDown.started += ctx => StartTap();
    }

    private void StartTap()
    {
        OnTap?.Invoke(PointerPosition());
    }

    public void Print(Vector2 position)
    {
        print(position);
        print(PointerPosition());
    }

    public Vector2 PointerPosition()
    {
        return _playerControls.Touch.PointerPosition.ReadValue<Vector2>();
    }

    private void OnEnable()
    {
        _playerControls.Enable();
    }

    private void OnDisable()
    {
        _playerControls.Disable();
    }
}
