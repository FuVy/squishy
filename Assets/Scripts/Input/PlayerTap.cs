using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTap : MonoBehaviour
{
    [SerializeField]
    private LayerMask _layerMask;
    [SerializeField]
    private float _spookRadius;
    [SerializeField, Range(0, 5)]
    private int _damage = 1;

    private Camera _camera;
    private void Awake()
    {
        _camera = Camera.main;
    }

    private void Start()
    {
        Setup();
    }

    private void Setup()
    {
        InputManager.Instance.OnTap += ShootRay;
    }

    private void ShootRay(Vector2 pointerPosition)
    {
        Ray ray = _camera.ScreenPointToRay(pointerPosition);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            var monster = hit.collider.GetComponent<Monster>();
            if (monster != null) //monster
            {
                var health = monster.GetComponent<Health>();
                health.ChangeHealth(-_damage);
            }

            ScareAt(hit);
        }
    }

    private void ScareAt(RaycastHit hit)
    {
        Collider[] hitColliders = Physics.OverlapSphere(hit.point, _spookRadius, _layerMask);

        foreach (Collider col in hitColliders)
        {
            col.GetComponent<Scarecrow>()?.GetScared(hit.point);
        }
    }

    private void OnDestroy()
    {
        InputManager.Instance.OnTap -= ShootRay;
    }
}
