using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
[DefaultExecutionOrder(10)]
public class LoseHandler : MonoBehaviour
{
    [SerializeField]
    private GameObject _loseMenu;
    [SerializeField]
    private GameObject _pauseButton;
    [SerializeField]
    private TMP_Text _record;
    private void Awake()
    {
        GameState.OnLose += Show;
    }
    private void OnDestroy()
    {
        GameState.OnLose -= Show;
    }

    private void Show()
    {
        _loseMenu.SetActive(true);
        _pauseButton.SetActive(false);
        _record.text = string.Format("Current game id: {0}, your time: {1}", RecordsHandler.CurrentSessionID, RecordsHandler.CurrentSessionTime);
    }

}
