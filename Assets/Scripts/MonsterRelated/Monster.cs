using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    [SerializeField]
    private MonsterData _data;
    
    public MonsterData Data => _data;

    [SerializeField]
    private SpecialAction[] _actions;

    [SerializeField]
    private MeshFilter _meshFilter;
    [SerializeField]
    private MeshRenderer _renderer;
    [SerializeField]
    private MeshCollider _collider;

    private void Awake()
    {
        Setup();
    }

    private void Setup()
    {
        ClearActions();
        ChangeMesh();
        InitActions();

        var health = GetComponent<Health>();
        health.SetMaxHealth(_data.MaxHealth);
        health.OnDeath += AddToQueue;

        GetComponent<MonsterMovement>().SetInitialSpeed(_data.Speed);
        GetComponent<Scarecrow>().SetScaredSpeed(_data.ScaredSpeed);
    }

    private void InitActions()
    {
        _actions = _data.Actions;
        foreach (SpecialAction action in _actions)
        {
            action.Init(this);
        }
    }

    private void ChangeMesh()
    {
        _meshFilter.sharedMesh = _data.Mesh;
        _renderer.material = _data.Material;
        _collider.sharedMesh = _data.Mesh;
    }

    public void SetData(MonsterData data)
    {
        _data = data;
        Setup();
    }

    public void ClearActions()
    {
        foreach (SpecialAction action in _actions)
        {
            action.RemoveFromEvent();
        }
    }

    private void OnDestroy()
    {
        GetComponent<Health>().OnDeath -= AddToQueue;
        MonsterPool.Instance.Destroy(this);
    }

    private void AddToQueue()
    {
        MonsterPool.Instance.Enqueue(this);
    }
}
