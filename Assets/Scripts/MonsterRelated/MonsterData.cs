using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable, CreateAssetMenu(fileName = "New Monster", menuName = "Monster")]
public class MonsterData : ScriptableObject
{
    [SerializeField]
    private string _name = "new";

    public string Name => _name;

    [SerializeField]
    private int _maxHealth = 1;

    public int MaxHealth => _maxHealth;

    [SerializeField]
    private float _speed = 3.5f;

    public float Speed => _speed;

    [SerializeField]
    private float _scaredSpeed = 7f;

    public float ScaredSpeed => _scaredSpeed;

    [SerializeField]
    private Mesh _mesh;

    public Mesh Mesh => _mesh;

    [SerializeField]
    private Material _material;

    public Material Material => _material;

    [SerializeField]
    private SpecialAction[] _actions;

    public SpecialAction[] Actions => _actions;
}
