using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterGenerator : MonoBehaviour
{
    [SerializeField]
    private float _delay = 3f;

    [SerializeField]
    private float _delayRandomness = 0.5f;

    [SerializeField]
    private int _totalQuantity = 10;

    [SerializeField]
    private MonsterData[] _monsterTypes;

    [SerializeField]
    private MonsterPool _pool;

    [SerializeField]
    private Monster _prefab;

    private Transform _transform;


    private void Awake()
    {
        _transform = transform;
        GameState.OnLose += StopGeneration;
        GameState.OnPlay += StartGeneration;
    }
    private void OnDestroy()
    {
        GameState.OnLose -= StopGeneration;
        GameState.OnPlay -= StartGeneration;
    }
    private void Start()
    {
        Init();
    }
    public void StartGeneration()
    {
        GenerateNext();
    }

    public void StopGeneration()
    {
        _pool.Refresh();
        CancelInvoke();
    }

    private void GenerateNext()
    {
        if (_pool.ActivatedMonsters() != _totalQuantity)
        {
            _pool.Dequeue();
            Invoke("GenerateNext", _delay + Random.Range(-_delayRandomness, _delayRandomness));
        }
        else
        {
            _pool.Refresh();
            GameState.Lose();
        }
    }

    private void Init()
    {
        for (int i = 0; i < _totalQuantity; i++)
        {
            var monster = Instantiate(_prefab, _transform);
            //RefreshMonster(monster);
            _pool.CreatedMonsters.Add(monster);
            _pool.Enqueue(monster);
        }
    }

    private void RefreshMonster(Monster monster)
    {
        monster.transform.position = GroundFinder.Instance.GetNavMeshRandomPoint();
        monster.SetData(_monsterTypes[Random.Range(0, _monsterTypes.Length)]);
        monster.name = monster.Data.Name;
    }

    public void ChangeMonstersData()
    {
        for (int i = 0; i < _pool.CreatedMonsters.Count; i++)
        {
            RefreshMonster(_pool.CreatedMonsters[i]);
        }
    }
}
