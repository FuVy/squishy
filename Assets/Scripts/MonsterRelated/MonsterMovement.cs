using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterMovement : MonoBehaviour
{
    [SerializeField, Range(0.3f, 2f)]
    private float _distanceCheckTime = 0.5f;

    [SerializeField]
    private NavMeshAgent _agent;

    private Vector3 _destinationPoint;

    private Transform _transform;

    private float _initialSpeed;

    public float AgentSpeed => _agent.speed;

    public Vector3 CurrentPosition => _transform.position;
    private void Awake()
    {
        Setup();
    }

    private void Setup()
    {
        _transform = transform;
        _agent.autoRepath = true;
        _initialSpeed = _agent.speed;
    }

    private void OnEnable()
    {
        //_transform.position = GroundFinder.Instance.GetNavMeshRandomPoint(); //!
        CancelInvoke();
        ChangeDestination();
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    public void ChangeDestination()
    {
        _destinationPoint = GroundFinder.Instance.GetNavMeshRandomPoint();
        _agent.SetDestination(_destinationPoint);
        Invoke("CheckDistance", _distanceCheckTime);
    }

    public void ChangeDestination(Vector3 point)
    {
        CancelInvoke();
        _agent.SetDestination(point);
    }

    private void CheckDistance()
    {
        float remainingTime = _agent.remainingDistance / _agent.speed;
        if (float.IsInfinity(remainingTime))
        {
            ChangeDestination();
        }
        Invoke("ChangeDestination", remainingTime);
    }

    public void SetSpeed(float value)
    {
        _agent.speed = value;
    }

    public void UpdateInitialSpeed()
    {
        SetSpeed(_initialSpeed);
    }

    public void SetInitialSpeed(float value)
    {
        _initialSpeed = value;
        UpdateInitialSpeed();
    }

    public void MoveAt(Vector3 direction)
    {
        _agent.Move(direction);
    }
}
