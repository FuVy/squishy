using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterPool : MonoBehaviour
{
    private Queue<Monster> _disabledMonsters = new Queue<Monster>();

    private List<Monster> _createdMonsters = new List<Monster>();

    public List<Monster> CreatedMonsters => _createdMonsters;

    public static MonsterPool Instance;
    private void Awake()
    {
        Instance = this;
        GameState.OnLose += Refresh;
    }
    private void OnDestroy()
    {
        GameState.OnLose -= Refresh;
    }
    public Monster Dequeue()
    {
        var monster = _disabledMonsters.Dequeue();
        monster.gameObject.SetActive(true);
        print(ActivatedMonsters());
        return monster;
    }

    public void Enqueue(Monster monster)
    {
        _disabledMonsters.Enqueue(monster);
        monster.gameObject.SetActive(false);
    }

    public void Destroy(Monster monster)
    {
        var monstersQueue = new List<Monster>(_disabledMonsters.ToArray());
        if (monstersQueue.Contains(monster))
        {
            monstersQueue.Remove(monster);
            _disabledMonsters.Clear();
            foreach(Monster entity in monstersQueue)
            {
                if (entity != null)
                {
                    _disabledMonsters.Enqueue(entity);
                }
            }
        }
        if (_createdMonsters.Contains(monster))
        {
            _createdMonsters.Remove(monster);
        }
    }

    public void Refresh()
    {
        _disabledMonsters.Clear();
        foreach (Monster monster in _createdMonsters)
        {
            Enqueue(monster);
        }
    }

    public int ActivatedMonsters()
    {
        int i = 0;
        foreach (Monster monster in _createdMonsters)
        {
            if (monster.gameObject.activeInHierarchy)
            {
                i++;
            }
        }
        return i;
    }
}
