using System.Collections;
using UnityEngine;

public class Scarecrow : MonoBehaviour
{
    [SerializeField]
    private MonsterMovement _movement;

    [Header("Scare cfg")]
    [SerializeField]
    private float _scareTime = 0.5f;
    [SerializeField]
    private float _scaredSpeed = 7f;
    [SerializeField]
    private float _distanceMultiplier = 3f;

    public void GetScared(Vector3 scarePosition)
    {
        Vector3 scareDestination = scarePosition - _movement.CurrentPosition;
        scareDestination *= - _distanceMultiplier;
        scareDestination += _movement.CurrentPosition;
        StartScare(scareDestination);
        Invoke("EndScare", _scareTime);
    }

    private void StartScare(Vector3 destination)
    {
        _movement.CancelInvoke();
        _movement.SetSpeed(_scaredSpeed);
        _movement.ChangeDestination(destination);
    }

    private void EndScare()
    {
        _movement.UpdateInitialSpeed();
        _movement.ChangeDestination();
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    public void SetScaredSpeed(float value)
    {
        _scaredSpeed = value;
    }
}
