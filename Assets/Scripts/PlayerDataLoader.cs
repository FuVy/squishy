using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class PlayerDataLoader
{
    private static string _path = Application.persistentDataPath + "/PlayerData.bin";

    private static PlayerData _data;

    public static PlayerData Data => _data;

    public static void Load()
    {
        if (Exists())
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(_path, FileMode.Open);
            _data = (PlayerData)formatter.Deserialize(stream);
            stream.Close();
        }
        else
        {
            CreateFile();
        }
    }

    public static void CreateFile()
    {
        File.Create(_path).Close();
        _data = new PlayerData 
        { 
            Records = new Dictionary<string, float>() 
        };
        Save();
    }

    public static void Save()
    {
        if (Exists())
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (var file = File.Open(_path, FileMode.OpenOrCreate))
            {
                formatter.Serialize(file, _data);
            }
        }
        else
        {
            CreateFile();
        }
    }

    public static bool Exists()
    {
        return File.Exists(_path);
    }
}
