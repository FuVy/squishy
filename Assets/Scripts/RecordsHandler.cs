using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class RecordsHandler : MonoBehaviour
{
    [SerializeField]
    private static PlayerData _data;
    [SerializeField]
    private TMP_Text _text;

    public static float CurrentSessionTime { get; private set; }
    public static string CurrentSessionID { get; private set; }

    private float _startingTime, _endingTime;
    
    private void Awake()
    {
        PlayerDataLoader.Load();
        GameState.OnPlay += StartCountdown;
        GameState.OnLose += EndCountdown;
        Refresh();
    }
    private void OnDestroy()
    {
        GameState.OnPlay -= StartCountdown;
        GameState.OnLose -= EndCountdown;
    }

    private void StartCountdown()
    {
        _startingTime = Time.time;
    }

    private void EndCountdown()
    {
        _endingTime = Time.time;
        CurrentSessionTime = _endingTime - _startingTime;
        AddRecord(CurrentSessionTime);
        Refresh();
    }
    private void Refresh()
    {
        _data = PlayerDataLoader.Data;
        OutputEveryRecord();
    }

    private void OutputEveryRecord()
    {
        _text.text = "Records:\n";
        int i = 1;
        foreach (var item in _data.Records.OrderByDescending(key => key.Value))
        {
            _text.text += string.Format("{0}: {1} - {2}\n", i, item.Key, item.Value);
            i++;
        }
    }

    public static void AddRecord(string name, float value)
    {
        _data.Records.Add(name, value);
        PlayerDataLoader.Save();
    }
    
    public static void AddRecord(float value)
    {
        CurrentSessionID = RandomString(8);
        AddRecord(CurrentSessionID, value);
    }
    
    private static string RandomString(int length)
    {
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return new string(Enumerable.Repeat(chars, length)
          .Select(s => s[Random.Range(0, s.Length)]).ToArray());
    }

    public void Clear()
    {
        PlayerDataLoader.CreateFile();
        PlayerDataLoader.Load();
        PlayerDataLoader.Save();
        _data = PlayerDataLoader.Data;
        _text.text = "Records:\n";
    }
}
