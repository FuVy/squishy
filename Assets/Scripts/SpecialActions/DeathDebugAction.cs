using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "SpecialActions/Debug")]
public class DeathDebugAction : SpecialAction
{
    private Health _health;
    private Monster _monster;
    private DeathDebug _debugger;

    public override void Init(MonoBehaviour mono)
    {
        _monster = mono.GetComponent<Monster>();
        _health = mono.GetComponent<Health>();
        _debugger = new DeathDebug(_monster.Data.Name);
        AddOnEvent();
    }

    public override void AddOnEvent()
    {
        _health.OnDeath += _debugger.Debug;
    }

    public override void RemoveFromEvent()
    {
        _health.OnDeath -= _debugger.Debug;
    }
}

public class DeathDebug
{
    private string _name;
    public void Debug()
    {
        MonoBehaviour.print(_name + " died...");
    }

    public DeathDebug(string name)
    {
        _name = name;
    }
}
