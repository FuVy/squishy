using UnityEngine;

public abstract class SpecialAction : ScriptableObject
{
    public abstract void Init(MonoBehaviour mono);

    public abstract void AddOnEvent();

    public abstract void RemoveFromEvent();
}
