using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(menuName = "SpecialActions/Wabble")]
public class Wabble : SpecialAction
{
    private Health _health;
    private Wabbler _wabbler;

    public override void Init(MonoBehaviour mono)
    {
        _health = mono.GetComponent<Health>();
        _wabbler = new Wabbler(_health.transform);
        AddOnEvent();
    }

    public override void AddOnEvent()
    {
        _health.OnHit += _wabbler.Wabble;
    }

    public override void RemoveFromEvent()
    {
        _health.OnHit -= _wabbler.Wabble;
    }
}

public class Wabbler
{
    private Transform _transform;
    public Wabbler(Transform transform)
    {
        _transform = transform;
    }

    public void Wabble()
    {
        _transform.DORewind();
        _transform.DOShakeScale(0.5f);
    }
}
