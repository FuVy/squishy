using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct PlayerData 
{
    public Dictionary<string, float> Records;
}
